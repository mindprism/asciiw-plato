## README

`C:\_\asciiw\._plato\assets\scripts`

- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts) _Destination Plato Scripts and Script Directories_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5CDesktop.ini) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw-plato/src/master/assets/scripts/) - bitbucket

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cbundles%5CREADME.md) `bundles` - Destination Plato Codemirror Scripts
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5CREADME.md) `vendor` - Destination Plato Vender Library Scripts

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Ccodemirror.markpopovertext.js) `codemirror.markpopovertext.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cplato-display.js) `plato-display.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cplato-file.js) `plato-file.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cplato-overview.js) `plato-overview.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cplato-sortable-file-list.js) `plato-sortable-file-list.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5CREADME.html) `README.html`

