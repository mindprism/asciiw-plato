## README

`C:\_\asciiw\._plato\assets\scripts\bundles`

- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cbundles) _Destination Plato Codemirror Scripts_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5CDesktop.ini) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cbundles%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw-plato/src/master/assets/scripts/bundles/) - bitbucket

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cbundles%5Ccodemirror.js) `codemirror.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cbundles%5Ccore-bundle.js) `core-bundle.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cbundles%5CREADME.html) `README.html`

