## README

`C:\_\asciiw\._plato\assets\scripts\vendor\codemirror`

- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror) _Destination Plato Codemirror Library Scripts_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5CDesktop.ini) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw-plato/src/master/assets/scripts/vendor/codemirror/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/asciiw/esdoc/file/._plato/assets/scripts/vendor/codemirror/codemirror.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/asciiw/plato/files/assets_scripts_vendor_codemirror_codemirror_js/index.html) - plato

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5CREADME.md) `util`

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Ccodemirror.js) `codemirror.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cjavascript.js) `javascript.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5CREADME.html) `README.html`

