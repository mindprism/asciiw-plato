## RMF-assets-scripts-vendor-codemirror-util

`C:\_\asciiw\._plato\assets\scripts\vendor\codemirror\util`

- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil) _summary_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5CDesktop.ini) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw-plato/src/master/assets/scripts/vendor/codemirror/util/) - bitbucket

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cclosetag.js) `closetag.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Ccolorize.js) `colorize.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Ccontinuecomment.js) `continuecomment.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Ccontinuelist.js) `continuelist.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cdialog.css) `dialog.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cdialog.js) `dialog.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cfoldcode.js) `foldcode.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cformatting.js) `formatting.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cjavascript-hint.js) `javascript-hint.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cloadmode.js) `loadmode.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cmatch-highlighter.js) `match-highlighter.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cmatchbrackets.js) `matchbrackets.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cmultiplex.js) `multiplex.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Coverlay.js) `overlay.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cpig-hint.js) `pig-hint.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Crunmode-standalone.js) `runmode-standalone.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Crunmode.js) `runmode.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Csearch.js) `search.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Csearchcursor.js) `searchcursor.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Csimple-hint.css) `simple-hint.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Csimple-hint.js) `simple-hint.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cxml-hint.js) `xml-hint.js`

