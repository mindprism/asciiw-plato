## README

`C:\_\asciiw\._plato\assets`

- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets) _Destination Plato styles, fonts and scripts_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5CDesktop.ini) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw-plato/src/master/assets/) - bitbucket

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Ccss%5CREADME.md) `css` - Destination Plato Styles
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cfont%5CREADME.md) `font` - Destination Plato Fonts - FontAwesome
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cscripts%5CREADME.md) `scripts` - Destination Plato Scripts and Script Directories

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5CREADME.html) `README.html`

