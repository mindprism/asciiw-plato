## README

`C:\_\asciiw\._plato\assets\font`

- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cfont) _Destination Plato Fonts - FontAwesome_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5CDesktop.ini) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cfont%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw-plato/src/master/assets/font/) - bitbucket

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cfont%5Cfontawesome-webfont.eot) `fontawesome-webfont.eot`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cfont%5Cfontawesome-webfont.svg) `fontawesome-webfont.svg`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cfont%5Cfontawesome-webfont.ttf) `fontawesome-webfont.ttf`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cfont%5Cfontawesome-webfont.woff) `fontawesome-webfont.woff`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Cfont%5CREADME.html) `README.html`

