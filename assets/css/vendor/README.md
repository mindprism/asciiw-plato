## README

`C:\_\asciiw\._plato\assets\css\vendor`

- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Ccss%5Cvendor) _Destination Plato Vender Styles_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Ccss%5CDesktop.ini) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Ccss%5Cvendor%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw-plato/src/master/assets/css/vendor/) - bitbucket

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Ccss%5Cvendor%5Cbootstrap.css) `bootstrap.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Ccss%5Cvendor%5Ccodemirror.css) `codemirror.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Ccss%5Cvendor%5Cfont-awesome.css) `font-awesome.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Ccss%5Cvendor%5Cmorris.css) `morris.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._plato%5Cassets%5Ccss%5Cvendor%5CREADME.html) `README.html`

